package org.example.framework.util;

import java.util.HashMap;
import java.util.Map;

public class Maps {
    private Maps() {
    }

    public static <K, V> Map<K, V> of() {
        return new HashMap<>();
    }

    public static <K, V> Map<K, V> of(K key, V value) {
        final HashMap<K, V> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2) {
        final HashMap<K, V> map = new HashMap<>();
        map.put(key1, value1);
        map.put(key2, value2);
        return map;
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2,
                                      K key3, V value3) {
        final HashMap<K, V> map = new HashMap<>();
        map.put(key1, value1);
        map.put(key2, value2);
        map.put(key3, value3);
        return map;
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2,
                                      K key3, V value3, K key4, V value4) {
        final HashMap<K, V> map = new HashMap<>();
        map.put(key1, value1);
        map.put(key2, value2);
        map.put(key3, value3);
        map.put(key4, value4);
        return map;
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2,
                                      K key3, V value3, K key4, V value4,
                                      K key5, V value5) {
        final HashMap<K, V> map = new HashMap<>();
        map.put(key1, value1);
        map.put(key2, value2);
        map.put(key3, value3);
        map.put(key4, value4);
        map.put(key5, value5);
        return map;
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2,
                                      K key3, V value3, K key4, V value4,
                                      K key5, V value5, K key6, V value6) {
        final HashMap<K, V> map = new HashMap<>();
        map.put(key1, value1);
        map.put(key2, value2);
        map.put(key3, value3);
        map.put(key4, value4);
        map.put(key5, value5);
        map.put(key6, value6);
        return map;
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2,
                                      K key3, V value3, K key4, V value4,
                                      K key5, V value5, K key6, V value6,
                                      K key7, V value7) {
        final HashMap<K, V> map = new HashMap<>();
        map.put(key1, value1);
        map.put(key2, value2);
        map.put(key3, value3);
        map.put(key4, value4);
        map.put(key5, value5);
        map.put(key6, value6);
        map.put(key7, value7);
        return map;
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2,
                                      K key3, V value3, K key4, V value4,
                                      K key5, V value5, K key6, V value6,
                                      K key7, V value7, K key8, V value8) {
        final HashMap<K, V> map = new HashMap<>();
        map.put(key1, value1);
        map.put(key2, value2);
        map.put(key3, value3);
        map.put(key4, value4);
        map.put(key5, value5);
        map.put(key6, value6);
        map.put(key7, value7);
        map.put(key8, value8);
        return map;
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2,
                                      K key3, V value3, K key4, V value4,
                                      K key5, V value5, K key6, V value6,
                                      K key7, V value7, K key8, V value8,
                                      K key9, V value9) {
        final HashMap<K, V> map = new HashMap<>();
        map.put(key1, value1);
        map.put(key2, value2);
        map.put(key3, value3);
        map.put(key4, value4);
        map.put(key5, value5);
        map.put(key6, value6);
        map.put(key7, value7);
        map.put(key8, value8);
        map.put(key9, value9);
        return map;
    }

    public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2,
                                      K key3, V value3, K key4, V value4,
                                      K key5, V value5, K key6, V value6,
                                      K key7, V value7, K key8, V value8,
                                      K key9, V value9, K key10, V value10) {
        final HashMap<K, V> map = new HashMap<>();
        map.put(key1, value1);
        map.put(key2, value2);
        map.put(key3, value3);
        map.put(key4, value4);
        map.put(key5, value5);
        map.put(key6, value6);
        map.put(key7, value7);
        map.put(key8, value8);
        map.put(key9, value9);
        map.put(key10, value10);
        return map;
    }
}

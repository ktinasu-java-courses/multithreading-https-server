package org.example.framework.handler;

import org.example.framework.http.Request;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

@FunctionalInterface
public interface Handler {
    void handle(final Request request, final OutputStream responseStream) throws IOException;

    static void internalServerError(final Request request, final OutputStream responseStream) throws IOException {
        final byte[] body = "Something bad happened".getBytes(StandardCharsets.UTF_8);
        responseStream.write((
                "HTTP/1.1 500 Internal Server Error\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: text/plain\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }

    static void notFoundHandler(final Request request, final OutputStream responseStream) throws IOException {
        final byte[] body = "Resource not found".getBytes(StandardCharsets.UTF_8);
        responseStream.write((
                "HTTP/1.1 404 Not Found\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: text/plain\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }

    static void methodNotAllowedHandler(final Request request, final OutputStream responseStream) throws IOException {
        final byte[] body = "Resource not found".getBytes(StandardCharsets.UTF_8);
        responseStream.write((
                "HTTP/1.1 405 Method Not Allowed\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: text/plain\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }

    static void unauthorized(final Request request, final OutputStream responseStream) throws IOException {
        final byte[] body = "Resource not found".getBytes(StandardCharsets.UTF_8);
        responseStream.write((
                "HTTP/1.1 401 Unauthorized\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: text/plain\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }

    static void badRequest(final Request request, final OutputStream responseStream) throws IOException {
        final byte[] body = "Resource not found".getBytes(StandardCharsets.UTF_8);
        responseStream.write((
                "HTTP/1.1 400 Bad Request\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: text/plain\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }
}

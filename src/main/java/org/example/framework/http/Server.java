package org.example.framework.http;

import lombok.Builder;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.exception.*;
import org.example.framework.form.parser.FormParser;
import org.example.framework.handler.Handler;
import org.example.framework.middleware.Middleware;
import org.example.framework.parser.Parser;
import org.example.framework.query.parser.QueryParser;
import org.example.framework.util.Bytes;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Builder
public class Server {
    private static final int MAX_REQUEST_LINE_AND_HEADERS_SIZE = 4096;
    private static final byte[] CRLF = new byte[]{'\r', '\n'};
    private static final byte[] CRLFCRLF = new byte[]{'\r', '\n', '\r', '\n'};
    private static final int MAX_CONTENT_LENGTH = 10 * 1024 * 1024;

    private final AtomicInteger workerCounter = new AtomicInteger();
    private final ExecutorService workers = Executors.newFixedThreadPool(64, r -> {
        final Thread worker = new Thread(r);
        worker.setName("worker-" + workerCounter.incrementAndGet());
        return worker;
    });

    @Singular
    private final List<Middleware> middlewares;
    @Singular
    private final Map<Pattern, Map<HttpMethods, Handler>> routes;
    @Builder.Default
    private final Handler notFoundHandler = Handler::notFoundHandler;
    @Builder.Default
    private final Handler methodNotAllowed = Handler::methodNotAllowedHandler;
    @Builder.Default
    private final Handler internalServerErrorHandler = Handler::internalServerError;
    @Builder.Default
    private final Handler unauthorized = Handler::unauthorized;
    @Builder.Default
    private final Handler badRequest = Handler::badRequest;

    private final AtomicBoolean closed = new AtomicBoolean(false);

    private ServerSocket serverSocket;

    public void stop() {
        log.info("Shutting down workers");
        closed.set(true);
        workers.shutdown();
        try {
            if (!workers.awaitTermination(5, TimeUnit.SECONDS)) {
                workers.shutdownNow();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        } finally {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void start(final int port) {
        new Thread(() -> {
            try {
                serveHTTPS(port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void serveHTTPS(final int port) throws IOException {
        final ServerSocketFactory socketFactory = SSLServerSocketFactory.getDefault();
        try {
            serverSocket = socketFactory.createServerSocket(port);
            final SSLServerSocket sslServerSocket = (SSLServerSocket) serverSocket;
            sslServerSocket.setEnabledProtocols(new String[]{"TLSv1.2"});
            sslServerSocket.setWantClientAuth(true);

            log.info("server listen on {}", port);
            while (!closed.get()) {
                try {
                    final Socket socket = serverSocket.accept();
                    workers.submit(() -> serve(socket));
                    if (closed.get()) {
                        break;
                    }
                } catch (IOException e) {
                    log.error("socket closed", e);
                } catch (Exception e) {
                    log.error("some error", e);
                }
            }
        } finally {
            serverSocket.close();
        }

    }

    private void serve(Socket socket) {
        try (
                final InputStream in = new BufferedInputStream(socket.getInputStream());
                final OutputStream out = socket.getOutputStream();
        ) {
            try {
                log.debug("client connected: {}:{}", socket.getInetAddress(), socket.getPort());

                Request request = new Request();

                final byte[] buffer = new byte[MAX_REQUEST_LINE_AND_HEADERS_SIZE];
                if (!in.markSupported()) {
                    throw new MarkNotSupportedException();
                }
                in.mark(MAX_REQUEST_LINE_AND_HEADERS_SIZE);

                in.read(buffer);
                final int requestLineEndIndex = Bytes.indexOf(buffer, CRLF);
                if (requestLineEndIndex == -1) {
                    throw new InvalidRequestStructureException("request line end index not found");
                }
                log.debug("request line end index: {}", requestLineEndIndex);
                final String requestLine = new String(buffer, 0, requestLineEndIndex, StandardCharsets.UTF_8);
                log.debug("request line: {}", requestLine);
                final String[] requestLineParts = requestLine.split("\\s+", 3);
                if (requestLineParts.length != 3) {
                    throw new InvalidRequestLineStructureException(requestLine);
                }

                request.setMethod(requestLineParts[0]);
                request.setHttpVersion(requestLineParts[2]);

                final String pathAndQuery = URLDecoder.decode(requestLineParts[1], StandardCharsets.UTF_8.name());

                final String[] pathAndQueryParts = pathAndQuery.split("\\?", 2);

                final String requestPath = pathAndQueryParts[0];
                request.setPath(requestPath);
                if (pathAndQueryParts.length == 2) {
                    request.setQuery(pathAndQueryParts[1]);
                }

                final int headersStartIndex = requestLineEndIndex + CRLF.length;
                final int headersEndIndex = Bytes.indexOf(buffer, CRLFCRLF, headersStartIndex);
                if (headersEndIndex == -1) {
                    throw new InvalidRequestStructureException("header end not found");
                }
                int lastProcessedIndex = headersStartIndex;
                int contentLength = 0;
                while (lastProcessedIndex < headersEndIndex - CRLF.length) {
                    final int currentHeaderEndIndex = Bytes.indexOf(buffer, CRLF, lastProcessedIndex);
                    final String currentHeaderLine = new String(buffer, lastProcessedIndex, currentHeaderEndIndex - lastProcessedIndex);
                    log.debug("current header line: {}", currentHeaderLine);
                    lastProcessedIndex = currentHeaderEndIndex + CRLF.length;

                    final String[] headerParts = currentHeaderLine.split(":\\s*", 2);
                    if (headerParts.length != 2) {
                        throw new InvalidHeaderLineStructureException(currentHeaderLine);
                    }
                    request.setHeader(headerParts[0], headerParts[1]);

                    if (!headerParts[0].equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH.value())) {
                        continue;
                    }
                    contentLength = Integer.parseInt(headerParts[1]);
                    log.debug("content-length: {}", contentLength);
                }

                if (contentLength < 0) {
                    throw new InvalidContentLengthException("Invalid content length");
                }

                if (contentLength > MAX_CONTENT_LENGTH) {
                    throw new RequestBodyTooLargeException("Request body is too large");
                }

                final int bodyStartIndex = headersEndIndex + CRLFCRLF.length;

                in.reset();
                in.skip(bodyStartIndex);
                final byte[] body = new byte[contentLength];
                final int bodyRead = in.read(body);

                if (bodyRead != contentLength) {
                    throw new BodyReadingException("Error while reading request body (body length != content length)");
                }

                request.setBody(body);

                final List<Parser> parsers = new ArrayList<>();
                parsers.add(new QueryParser());
                parsers.add(new FormParser());
                for (final Parser parser : parsers) {
                    request = parser.parse(request);
                }

                log.debug("request: method: {} \n path: {}\n query {}\n httpVersion {}\n headers {}\n " +
                                "body {}\n queryparams {}\n formparams {}", request.getMethod(), request.getPath(),
                        request.getQuery(), request.getHttpVersion(), request.getHeaders(), request.getBody(),
                        request.getQueryParams(), request.getFormParams());

                in.reset();


                for (final Middleware middleware : middlewares) {
                    middleware.handle(socket, request);
                }

                Map<HttpMethods, Handler> methodToHandlers = null;
                for (final Map.Entry<Pattern, Map<HttpMethods, Handler>> entry : routes.entrySet()) {
                    final Matcher matcher = entry.getKey().matcher(requestPath);
                    if (!matcher.matches()) {
                        continue;
                    }
                    request.setPathMatcher(matcher);
                    methodToHandlers = entry.getValue();
                }

                try {
                    if (methodToHandlers == null) {
                        throw new MethodNotAllowedException(request.getMethod());
                    }

                    final Handler handler = methodToHandlers.get(HttpMethods.valueOf(request.getMethod()));
                    if (handler == null) {
                        throw new ResourceNotFoundException(requestPath);
                    }

                    handler.handle(request, out);
                } catch (BadRegisterDataException e) {
                    log.error("can't register", e);
                    badRequest.handle(new Request(), out);
                } catch (MethodNotAllowedException e) {
                    log.error("request method not allowed", e);
                    methodNotAllowed.handle(request, out);
                } catch (ResourceNotFoundException e) {
                    log.error("can't found request", e);
                    notFoundHandler.handle(request, out);
                } catch (Exception e) {
                    log.error("can't handle request", e);
                    internalServerErrorHandler.handle(request, out);
                }


            } catch (AuthenticationException e) {
                log.error("can't authenticate", e);
                unauthorized.handle(new Request(), out);
            } catch (Exception e) {
                log.error("can't handle request", e);
                internalServerErrorHandler.handle(new Request(), out);
            }
        } catch (Exception e) {
            log.error("some error", e);
        }
    }
}

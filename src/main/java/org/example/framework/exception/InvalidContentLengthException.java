package org.example.framework.exception;

public class InvalidContentLengthException extends RuntimeException {
    public InvalidContentLengthException() {
    }

    public InvalidContentLengthException(String message) {
        super(message);
    }

    public InvalidContentLengthException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidContentLengthException(Throwable cause) {
        super(cause);
    }

    public InvalidContentLengthException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

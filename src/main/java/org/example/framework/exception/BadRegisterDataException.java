package org.example.framework.exception;

public class BadRegisterDataException extends RuntimeException {
    public BadRegisterDataException() {
    }

    public BadRegisterDataException(String message) {
        super(message);
    }

    public BadRegisterDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRegisterDataException(Throwable cause) {
        super(cause);
    }

    public BadRegisterDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

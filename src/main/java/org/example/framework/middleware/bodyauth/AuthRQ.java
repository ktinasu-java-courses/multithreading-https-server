package org.example.framework.middleware.bodyauth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthRQ {
    private String username;
    private String password;
}

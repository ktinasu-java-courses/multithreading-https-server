package org.example.framework.auth;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LoginPasswordAuthentication implements Authentication {
    @Getter
    private final String login;
    private final String password;

    @Override
    public Object getCredentials() {
        return password;
    }
}

package org.example.framework.auth;

public interface Authentication {
    Object getLogin();

    Object getCredentials();
}

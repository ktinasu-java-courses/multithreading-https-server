package org.example.framework.auth;

public interface Authenticator {
    boolean authenticate(Authentication request);
}

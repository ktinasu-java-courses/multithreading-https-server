package org.example.framework.query.parser;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.http.Request;
import org.example.framework.parser.Parser;
import org.example.framework.query.exception.QueryParseException;
import org.example.framework.util.Maps;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
class QueryParserTest {
    @Test
    void shouldNotParseIfInvalidQuery() {
        Request request = new Request();
        final Parser parser = new QueryParser();
        request.setQuery("name=phone87=name");

        assertThrows(QueryParseException.class, () -> parser.parse(request));
    }

    @Test
    void shouldNotParseIfInvalidQueryParts() {
        Request request = new Request();
        final Parser parser = new QueryParser();
        request.setQuery("name=phone&phone=87=value");

        assertThrows(QueryParseException.class, () -> parser.parse(request));
    }

    @Test
    void shouldNotParseIfBodyNull() {
        Request request = new Request();
        final Parser parser = new QueryParser();
        request.setQuery(null);

        request = parser.parse(request);

        assertEquals(new LinkedHashMap<>(), request.getQueryParams());
    }

    @Test
    void shouldParseIfNullValue() {
        Request request = new Request();
        final Parser parser = new QueryParser();
        request.setQuery("name=ivan&phone=7-9xx-xxx-xx-xx&emoji=");
        request = parser.parse(request);


        final List<String> nameValues = new ArrayList<>();
        nameValues.add("ivan");
        final List<String> phoneValues = new ArrayList<>();
        phoneValues.add("7-9xx-xxx-xx-xx");
        final List<String> emojiValues = new ArrayList<>();
        emojiValues.add("");
        final Map<Object, Object> expected = new LinkedHashMap<>(
                Maps.of("name", nameValues,
                        "phone", phoneValues,
                        "emoji", emojiValues)
        );

        final Map<String, List<String>> actual = request.getQueryParams();

        assertEquals(expected, actual);
    }

    @Test
    void shouldParse() {
        Request request = new Request();
        final Parser parser = new QueryParser();
        request.setQuery("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya");
        request = parser.parse(request);


        final List<String> nameValues = new ArrayList<>();
        nameValues.add("ivan");
        nameValues.add("vanya");
        final List<String> phoneValues = new ArrayList<>();
        phoneValues.add("7-9xx-xxx-xx-xx");
        final Map<Object, Object> expected = new LinkedHashMap<>(
                Maps.of("name", nameValues,
                        "phone", phoneValues)
        );

        final Map<String, List<String>> actual = request.getQueryParams();

        assertEquals(expected, actual);
    }

}

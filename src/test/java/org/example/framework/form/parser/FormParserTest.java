package org.example.framework.form.parser;

import org.example.framework.form.exception.FormParseException;
import org.example.framework.http.Request;
import org.example.framework.parser.Parser;
import org.example.framework.util.Maps;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FormParserTest {
    @Test
    void shouldNotParseIfContentTypeNull() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setHeaders(
                Maps.of("content-length", "46",
                "connection", "close")
        );

        request = parser.parse(request);

        assertEquals(new LinkedHashMap<>(), request.getFormParams());
    }

    @Test
    void shouldNotParseIfNotForm() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setBody("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya".getBytes(StandardCharsets.UTF_8));
        request.setHeaders(
                Maps.of("content-type", "multipart/form-data")
        );
        request = parser.parse(request);

        assertEquals(new LinkedHashMap<>(), request.getFormParams());
    }

    @Test
    void shouldNotParseIfBodyNull() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setBody(null);
        request.setHeaders(
                Maps.of("CONTENT-TYPE", "application/x-www-form-urlencoded")
        );

        request = parser.parse(request);

        assertEquals(new LinkedHashMap<>(), request.getFormParams());
    }

    @Test
    void shouldNotParseIfInvalidForm() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setBody("name=phone=phone=87value".getBytes(StandardCharsets.UTF_8));
        request.setHeaders(
                Maps.of("CONTENT-TYPE", "application/x-www-form-urlencoded")
        );

        assertThrows(FormParseException.class, () -> parser.parse(request));
    }

    @Test
    void shouldNotParseIfInvalidFormParts() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setBody("name=phone&phone=87=value".getBytes(StandardCharsets.UTF_8));
        request.setHeaders(
                Maps.of("CONTENT-TYPE", "application/x-www-form-urlencoded")
        );

        assertThrows(FormParseException.class, () -> parser.parse(request));
    }

    @Test
    void shouldParseIfNullValue() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setBody("name=ivan&phone=7-9xx-xxx-xx-xx&emoji=".getBytes(StandardCharsets.UTF_8));
        request.setHeaders(
                Maps.of("CONTENT-TYPE", "application/x-www-form-urlencoded")
        );

        request = parser.parse(request);


        final List<String> nameValues = new ArrayList<>();
        nameValues.add("ivan");
        final List<String> phoneValues = new ArrayList<>();
        phoneValues.add("7-9xx-xxx-xx-xx");
        final List<String> emojiValues = new ArrayList<>();
        emojiValues.add("");
        final Map<Object, Object> expected = new LinkedHashMap<>(
                Maps.of("name", nameValues,
                        "phone", phoneValues,
                        "emoji", emojiValues)
        );

        final Map<String, List<String>> actual = request.getFormParams();

        assertEquals(expected, actual);
    }

    @Test
    void shouldParse() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setBody("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya".getBytes(StandardCharsets.UTF_8));
        request.setHeaders(
                Maps.of("CONTENT-TYPE", "application/x-www-form-urlencoded")
        );

        request = parser.parse(request);


        final List<String> nameValues = new ArrayList<>();
        nameValues.add("ivan");
        nameValues.add("vanya");
        final List<String> phoneValues = new ArrayList<>();
        phoneValues.add("7-9xx-xxx-xx-xx");
        final Map<Object, Object> expected = new LinkedHashMap<>(
                Maps.of("name", nameValues,
                        "phone", phoneValues)
        );

        final Map<String, List<String>> actual = request.getFormParams();

        assertEquals(expected, actual);
    }
}

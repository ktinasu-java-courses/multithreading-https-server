package org.example.framework.http;

import org.example.framework.form.parser.FormParser;
import org.example.framework.parser.Parser;
import org.example.framework.query.parser.QueryParser;
import org.example.framework.util.Maps;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class RequestTest {

    @Test
    void shouldGetQueryParam() {
        Request request = new Request();
        final Parser parser = new QueryParser();
        request.setQuery("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya");
        request = parser.parse(request);

        final Optional<String> expected = Optional.of("ivan");
        final Optional<String> actual = request.getQueryParam("name");

        assertEquals(expected, actual);
    }

    @Test
    void shouldGetQueryNullParam() {
        Request request = new Request();
        final Parser parser = new QueryParser();
        request.setQuery("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya");
        request = parser.parse(request);

        final Optional<String> expected = Optional.empty();
        final Optional<String> actual = request.getQueryParam("email");

        assertEquals(expected, actual);
    }

    @Test
    void shouldGetQueryParams() {
        Request request = new Request();
        final Parser parser = new QueryParser();
        request.setQuery("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya");
        request = parser.parse(request);

        final List<String> expected = new ArrayList<>();
        expected.add("ivan");
        expected.add("vanya");
        final List<String> actual = request.getQueryParams("name");

        assertEquals(expected, actual);
    }

    @Test
    void shouldGetQueryNullParams() {
        Request request = new Request();
        final Parser parser = new QueryParser();
        request.setQuery("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya");
        request = parser.parse(request);

        final List<String> actual = request.getQueryParams("email");

        assertNull(actual);
    }

    @Test
    void shouldGetAllQueryParams() {
        Request request = new Request();
        final Parser parser = new QueryParser();
        request.setQuery("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya");
        request = parser.parse(request);

        final List<String> strings = new ArrayList<>();
        strings.add("ivan");
        strings.add("vanya");
        final List<String> strings1 = new ArrayList<>();
        strings1.add("7-9xx-xxx-xx-xx");

        final Map<String, List<String>> expected = new HashMap<>(
                Maps.of("name", strings,
                        "phone", strings1)
        );

        final Map<String, List<String>> actual = request.getAllQueryParams();

        assertEquals(expected, actual);
    }


    @Test
    void shouldGetFormParam() {
        Request request = new Request();
        final Parser parser = new FormParser();

        request.setBody("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya".getBytes(StandardCharsets.UTF_8));
        request.setHeaders(
                Maps.of("CONTENT-TYPE", "application/x-www-form-urlencoded")
        );

        request = parser.parse(request);

        final Optional<String> expected = Optional.of("ivan");
        final Optional<String> actual = request.getFormParam("name");

        assertEquals(expected, actual);
    }

    @Test
    void shouldGetNullFormParam() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setBody("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya".getBytes(StandardCharsets.UTF_8));
        request.setHeaders(
                Maps.of("CONTENT-TYPE", "application/x-www-form-urlencoded")
        );

        request = parser.parse(request);

        final Optional<String> expected = Optional.empty();
        final Optional<String> actual = request.getFormParam("email");

        assertEquals(expected, actual);
    }

    @Test
    void shouldGetFormParams() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setBody("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya".getBytes(StandardCharsets.UTF_8));
        request.setHeaders(
                Maps.of("CONTENT-TYPE", "application/x-www-form-urlencoded")
        );
        request = parser.parse(request);

        final List<String> expected = new ArrayList<>();
        expected.add("ivan");
        expected.add("vanya");
        final List<String> actual = request.getFormParams("name");

        assertEquals(expected, actual);
    }

    @Test
    void shouldGetNullFormParams() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setBody("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya".getBytes(StandardCharsets.UTF_8));
        request.setHeaders(
                Maps.of("CONTENT-TYPE", "application/x-www-form-urlencoded")
        );
        request = parser.parse(request);

        final List<String> actual = request.getFormParams("email");

        assertNull(actual);
    }

    @Test
    void shouldGetAllFormParams() {
        Request request = new Request();
        final Parser parser = new FormParser();
        request.setBody("name=ivan&phone=7-9xx-xxx-xx-xx&name=vanya".getBytes(StandardCharsets.UTF_8));
        request.setHeaders(
                Maps.of("CONTENT-TYPE", "application/x-www-form-urlencoded")
        );
        request = parser.parse(request);

        final List<String> nameValues = new ArrayList<>();
        nameValues.add("ivan");
        nameValues.add("vanya");
        final List<String> phoneValues = new ArrayList<>();
        phoneValues.add("7-9xx-xxx-xx-xx");

        final HashMap<String, List<String>> expected = new HashMap<>(
                Maps.of("name", nameValues,
                        "phone", phoneValues)
        );
        final Map<String, List<String>> actual = request.getAllFormParams();

        assertEquals(expected, actual);
    }
}